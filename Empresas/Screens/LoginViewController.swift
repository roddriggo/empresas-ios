//
//  LoginViewController.swift
//  Empresas
//
//  Created by Rodrigo Lourenço on 05/01/22.
//

import UIKit
import NotificationBannerSwift

class LoginViewController: UIViewController {
    
    @IBOutlet private weak var txtEmail: UITextField!
    @IBOutlet private weak var txtPassword: UITextField!
    
    //-----------------------------------------------------------------------
    //  MARK: - Custom methods
    //-----------------------------------------------------------------------
    
    @IBAction private func makeLogin() {
        
        RequestManager.makeLogin(email: txtEmail.text ?? "", password: txtPassword.text ?? "") { success in
            
            if success {
                self.showHome()
            }else{
                let banner = NotificationBanner(title: "Dados inválidos!",
                                                subtitle: "Por favor, corrija os dados e tente novamente.",
                                                style: .danger)
                banner.show()
            }
        }
    }
    
    private func showHome() {
        let enterprisesVC = self.storyboard?.instantiateViewController(withIdentifier: "ListView") as! ListViewController
        self.navigationController?.pushViewController(enterprisesVC, animated: true)
    }
}
