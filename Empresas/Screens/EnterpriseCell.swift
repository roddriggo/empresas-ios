//
//  EnterpriseCell.swift
//  Empresas
//
//  Created by Rodrigo Lourenço on 06/01/22.
//

import UIKit
import Kingfisher

class EnterpriseCell: UITableViewCell {
    
    @IBOutlet private weak var lblCell: UILabel!
    @IBOutlet private weak var imgCell: UIImageView!
        
    func loadUI(item: Enterprise) {
        
        lblCell.text = item.name
        
        if let imgUrl = URL(string: RequestManager.photoURL + item.photo) {
            imgCell.kf.setImage(with: imgUrl)
        }
    }
}
