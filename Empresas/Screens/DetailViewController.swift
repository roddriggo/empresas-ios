//
//  DetailViewController.swift
//  Empresas
//
//  Created by Rodrigo Lourenço on 05/01/22.
//

import UIKit
import Kingfisher

class DetailViewController: UIViewController {
    
    var object: Enterprise!
       
    @IBOutlet private weak var lblType: UILabel!
    @IBOutlet private weak var lblEnterprise: UILabel!
    @IBOutlet private weak var lblName: UILabel!
    @IBOutlet private weak var txtDescription: UITextView!
    @IBOutlet private weak var imgEnterprise: UIImageView?
    
    //-----------------------------------------------------------------------
    //  MARK: - UIViewController
    //-----------------------------------------------------------------------
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = object.name
        
        self.lblType.text = object.type?.name
        self.lblName.text = object.name
        self.lblEnterprise.text = object.name
        self.txtDescription.text = object.description
        
        if let imgUrl = URL(string: RequestManager.photoURL + object.photo) {
            imgEnterprise?.kf.setImage(with: imgUrl)
        }
    }
    
    @IBAction func buttonDismiss(_ sender: Any) {
        
        self.navigationController?.popViewController(animated: true)
    }
}
