//
//  ListViewController.swift
//  Empresas
//
//  Created by Rodrigo Lourenço on 05/01/22.
//

import UIKit
import Kingfisher

class ListViewController: UIViewController,
                          UITableViewDelegate,
                          UITableViewDataSource,
                          UISearchBarDelegate {
    
    private var items: [Enterprise] = []
    private var itemsOriginal: [Enterprise] = []
    
    @IBOutlet private weak var tbList: UITableView!
    @IBOutlet private weak var sbSearch: UISearchBar!
    
    
    //-----------------------------------------------------------------------
    //  MARK: - UIViewController
    //-----------------------------------------------------------------------
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        self.loadUI()
    }
    
    //-----------------------------------------------------------------------
    //  MARK: - UISearchBar Delegate
    //-----------------------------------------------------------------------
    
    func searchBar(_ searchBar: UISearchBar,
                   shouldChangeTextIn range: NSRange,
                   replacementText text: String) -> Bool {
        
        if text == "\n" {
            return true
        }
        
        let string = ((searchBar.text ?? "") + text).lowercased()
        
        items = itemsOriginal.filter({$0.name.lowercased().contains(string)})
        
        self.tbList.reloadData()
        
        return true
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        
        if searchText.isEmpty {
            
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.15) {
                
                self.items = self.itemsOriginal
                
                self.tbList.reloadData()
                
                searchBar.resignFirstResponder()
            }
        }
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchBar.resignFirstResponder()
    }
    
    //-----------------------------------------------------------------------
    //  MARK: - UITableView Delegate / Datasource
    //-----------------------------------------------------------------------
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return items.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "EnterpriseCell", for: indexPath) as! EnterpriseCell
        
        cell.loadUI(item: items[indexPath.row])
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
        self.showDetail(item: items[indexPath.row])
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 90
    }
    
    //-----------------------------------------------------------------------
    //  MARK: - Custom methods
    //-----------------------------------------------------------------------
    
    private func showDetail(item: Enterprise) {
        
        let detailVC = self.storyboard?.instantiateViewController(withIdentifier: "DetailView") as! DetailViewController
        detailVC.object = item
        
        self.navigationController?.pushViewController(detailVC, animated: true)
    }
    
    private func loadUI() {
        
        if itemsOriginal.count == 0 {
            
            RequestManager.loadEnterprises { items in
                
                self.items = items
                self.itemsOriginal = items
                
                self.tbList.reloadData()
            }
        }
    }
    
    @IBAction private func btBackLogin(_ sender: Any) {
        
        self.navigationController?.popViewController(animated: true)
    }
}
