//
//  RequestManager.swift
//  Empresas
//
//  Created by Rodrigo Lourenço on 05/01/22.
//

import Foundation

class RequestManager {
    
    static let photoURL: String = "https://empresas.ioasys.com.br"
    private static let serverURL: String = "https://empresas.ioasys.com.br/api/v1"
    
    //--------------------------------------------
    //  MARK: - Login
    //--------------------------------------------
    
    static func makeLogin(email: String, password: String, completion: @escaping (_ success: Bool) -> Void) {
        
        let parameters: Dictionary<String, String> = ["email": email,
                                                      "password": password]

        let request = NSMutableURLRequest()
        request.httpMethod = "POST"
        request.url = URL(string: serverURL + "/users/auth/sign_in")!
        
        request.setValue("*/*", forHTTPHeaderField: "Accept")
        request.setValue("no-cache", forHTTPHeaderField: "Cache-Control")
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        
        request.httpBody = try? JSONSerialization.data(withJSONObject: parameters, options: .prettyPrinted)

        URLSession.shared.dataTask(with: request as URLRequest) { data, response, _ in
            
            if let response = response as? HTTPURLResponse {
                
                let uid = response.value(forHTTPHeaderField: "uid")
                let client = response.value(forHTTPHeaderField: "client")
                let token = response.value(forHTTPHeaderField: "access-token")
                
                UserDefaults.standard.set(uid, forKey: "uid")
                UserDefaults.standard.set(client, forKey: "client")
                UserDefaults.standard.set(token, forKey: "access-token")
            }
            
            if let data = data {
                
                do {
                    let parse = try JSONDecoder().decode(Auth.self, from: data)
                    
                    DispatchQueue.main.async {
                        completion(parse.success)
                    }
                }catch{
                    DispatchQueue.main.async {
                        completion(false)
                    }
                }
            }
        }.resume()
    }
    
    //--------------------------------------------
    //  MARK: - Enterprises
    //--------------------------------------------
    
    static func loadEnterprises(completion: @escaping (_ items: [Enterprise]) -> Void) {
        
        let request = NSMutableURLRequest()
        request.httpMethod = "GET"
        request.url = URL(string: serverURL + "/enterprises")!
        
        request.setValue("*/*", forHTTPHeaderField: "Accept")
        request.setValue("no-cache", forHTTPHeaderField: "Cache-Control")
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")

        request.addValue(UserDefaults.standard.string(forKey: "uid") ?? "", forHTTPHeaderField: "uid")
        request.addValue(UserDefaults.standard.string(forKey: "client") ?? "", forHTTPHeaderField: "client")
        request.addValue(UserDefaults.standard.string(forKey: "access-token") ?? "", forHTTPHeaderField: "access-token")
        
        URLSession.shared.dataTask(with: request as URLRequest) { data, _, _ in
            
            if let data = data {
                
                do {
                    let parse = try JSONDecoder().decode(Enterprises.self, from: data)
                    
                    DispatchQueue.main.async {
                        completion(parse.list)
                    }
                }catch{
                    
                    print(error)
                    
                    DispatchQueue.main.async {
                        completion([])
                    }
                }
            }
        }.resume()
    }
}
