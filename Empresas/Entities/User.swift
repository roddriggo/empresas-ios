//
//  User.swift
//  Empresas
//
//  Created by Rodrigo Lourenço on 05/01/22.
//

import Foundation

struct User: Codable {
    
    var objectId: Int
    var name: String
    var email: String
    
    enum CodingKeys: String, CodingKey {
        case objectId = "id"
        case name = "investor_name"
        case email
    }
}
