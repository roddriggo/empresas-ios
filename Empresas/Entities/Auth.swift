//
//  Auth.swift
//  Empresas
//
//  Created by Rodrigo Lourenço on 05/01/22.
//

import Foundation

struct Auth: Codable {
    
    var user: User
    var success: Bool
    
    enum CodingKeys: String, CodingKey {
        case user = "investor"
        case success
    }
}
