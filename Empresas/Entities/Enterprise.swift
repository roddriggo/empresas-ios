//
//  Enterprise.swift
//  Empresas
//
//  Created by Rodrigo Lourenço on 05/01/22.
//

import Foundation

struct Enterprises: Codable {
    
    var list: [Enterprise]
    
    enum CodingKeys: String, CodingKey {
        case list = "enterprises"
    }
}

struct Enterprise: Codable {
    
    var objectId: Int
    var name: String
    var photo: String
    var description: String
    var city: String
    var country: String
    var type: EnterpriseType?
    
    enum CodingKeys: String, CodingKey {
        case objectId = "id"
        case name = "enterprise_name"
        case photo
        case description
        case city
        case country
        case type = "enterprise_type"
    }
}

struct EnterpriseType: Codable {
    
    var objectId: Int
    var name: String
    
    enum CodingKeys: String, CodingKey {
        case objectId = "id"
        case name = "enterprise_type_name"
    }
}
